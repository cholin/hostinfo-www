#!/usr/bin/python2

import yaml
import os
import jinja2

# config
DATAPATH = "./data"
WWW_DIR = "./www"

class Renderer:
    def __init__(self, output_dir):
        self.env = jinja2.Environment(
            loader = jinja2.FileSystemLoader('./templates'))
        self.output_dir = output_dir
        self.templates = {}

    def _render(self, template, filename, **kwargs):
        if template not in self.templates:
            self.templates[template] = self.env.get_template(template)

        with open(os.path.join(self.output_dir, filename), "w+") as output:
            output.write(self.templates[template].render(**kwargs))

    def host(self, host, hostdata):
        print("Rendering: %s" % host)
        hostdata['url'] = "%s.html" % host
        self._render('host.html', hostdata['url'], **hostdata)

    def index(self, data):
        print("Rendering: index")
        self._render('index.html', 'index.html', hosts=data)

def load_yaml(*args):
    with open(os.path.join(DATAPATH, *args)) as infile:
        return yaml.load(infile)

def main():
    renderer = Renderer(WWW_DIR)
    hosts = load_yaml("metadata", "hosts")
    data = []

    for host in sorted(hosts['hosts']):
        hostdata = load_yaml(host)
        renderer.host(host, hostdata)
        data.append(hostdata)

    renderer.index(data)

if __name__ == '__main__':
    main()
